﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Contract
{
    public interface IUstawCzas
    {
        int Czas { get; set; }

        void UstawCzas(int czas);
        void Wyczysc();
    }
}
