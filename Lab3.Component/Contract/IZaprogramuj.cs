﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Contract
{
    public interface IZaprogramuj
    {
        int Moc { get; set; }

        void UstawMoc(int moc);
        void Wyczysc();
    }
}
