﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Contract
{
    /// <summary>
    /// Interfejs dla Domieszki - DostarczaczZasilania
    /// </summary>
    public interface IMZasilanie
    {
        bool CzyPlyniePrad { get; set; }
    }
}
