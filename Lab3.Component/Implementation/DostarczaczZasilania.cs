﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Implementation
{
    /// <summary>
    /// Domieszka - DostarczaczZasilania
    /// </summary>
    public static class DostarczaczZasilania
    {
        public static void WlaczZasilanie(this IMZasilanie zasilacz)
        {
            zasilacz.CzyPlyniePrad = true;
            Console.WriteLine("Włączam zasilanie modułu {0}!", zasilacz.GetType());
        }
    }
}
