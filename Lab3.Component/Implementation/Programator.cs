﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Implementation
{
    /// <summary>
    /// Komponent/Kontener (FASADA 2) - Programator
    /// </summary>
    public class Programator : IMZasilanie, IZaprogramuj,  IUstawCzas
    {
        private IUstawCzas czasomierz = new Czasomierz();
        private IMZasilanie zasilacz = new Zasilacz();

        int moc1 = 400;
        int czas = 60;
        bool czyDrzwiZamkniete = true;
        bool czyInstalacjaBezpieczna = true;

        public Programator()
        {            
            ((IMZasilanie)czasomierz).WlaczZasilanie();
            ((IMZasilanie)zasilacz).WlaczZasilanie();
            ((IMZasilanie)this).WlaczZasilanie();
        }

        public void WlaczOgrzewanie()
        {
                UstawMoc(moc1);
                ((IUstawCzas)this).UstawCzas(czas);
                ((IUstawCzas)this).Wyczysc();
                ((IZaprogramuj)this).Wyczysc();
        }

        public static IMZasilanie Zwroc1(Programator fasada)
        {
            return new Programator();
        }

        public static IUstawCzas Zwroc2(Programator fasada)
        {
            return (IUstawCzas)new Programator();
        }

        public static IZaprogramuj Zwroc3(Programator fasada)
        {
            return (IZaprogramuj)new Programator();
        }

        #region IMPLEMENTACJA IMZasilanie
        bool czyPlyniePrad = false;
        public bool CzyPlyniePrad
        {
            get
            {
                return czyPlyniePrad;
            }
            set
            {
                czyPlyniePrad = value;
            }
        }
        #endregion

        #region IMPLEMENTACJA IZaprogramuj
        int moc = 0;
        public int Moc
        {
            get
            {
                return moc;
            }
            set
            {
                moc = value;
            }
        }

        public void UstawMoc(int moc)
        {
            Moc = moc;
            Console.WriteLine("Moc ustawiona na {0}", Moc);
        }

        void IZaprogramuj.Wyczysc()
        {
            Moc = 0;
            Console.WriteLine("Moc wyczyszczona!");
        }
        #endregion


        #region IMPLEMENTACJA IUstawCzas
        public int Czas
        {
            get
            {
                return czasomierz.Czas;
            }
            set
            {
                czasomierz.Czas = value;
            }
        }

        public void UstawCzas(int czas)
        {
            czasomierz.UstawCzas(czas);
        }

        void IUstawCzas.Wyczysc()
        {
            ((IUstawCzas)czasomierz).Wyczysc();
        }
        #endregion

    }
}
