﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Implementation
{
    /// <summary>
    /// Komponent - Czasomierz
    /// </summary>
    class Czasomierz : IMZasilanie, IUstawCzas
    {
        #region IMPLEMENTACJA IGrzej
        bool czyPlyniePrad = false;
        public bool CzyPlyniePrad
        {
            get
            {
                return czyPlyniePrad;
            }
            set
            {
                czyPlyniePrad = value;
            }
        }
        #endregion

        #region IMPLEMENTACJA IUstawCzas
        int czas = 0;
        public int Czas
        {
            get
            {
                return czas;
            }
            set
            {
                czas = value;
            }
        }

        public void UstawCzas(int czas)
        {
            Czas = czas;
            Console.WriteLine("Czas ustawiony na {0}!", Czas);
        }

        void IUstawCzas.Wyczysc()
        {
            Czas = 0;
            Console.WriteLine("Czas wyczyszczony!");
        }
        #endregion
    }
}
