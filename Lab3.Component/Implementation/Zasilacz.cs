﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.Implementation
{
    /// <summary>
    /// Komponent - Zasilacz
    /// </summary>
    class Zasilacz : IMZasilanie
    {
        #region IMPLEMENTACJA IMZasilanie
        bool czyPlyniePrad = false;
        public bool CzyPlyniePrad
        {
            get
            {
                return czyPlyniePrad;
            }
            set
            {
                czyPlyniePrad = value;
            }
        }
        #endregion
    }
}
