﻿using Lab3.Contract;
using Lab3.Implementation;
using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IUstawCzas);
        public static Type I2 = typeof(IZaprogramuj);
        public static Type I3 = typeof(IMZasilanie);

        public static Type Component = typeof(Programator);

        public static GetInstance GetInstanceOfI1 = Component => Programator.Zwroc1(new Programator());
        public static GetInstance GetInstanceOfI2 = Component => Programator.Zwroc2(new Programator());
        public static GetInstance GetInstanceOfI3 = Component => Programator.Zwroc3(new Programator());
        
        #endregion

        #region P2

        public static Type Mixin = typeof(DostarczaczZasilania);
        public static Type MixinFor = typeof(IMZasilanie);

        #endregion
    }
}
